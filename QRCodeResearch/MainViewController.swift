//
//  MainViewController.swift
//  QRCodeResearch
//
//  Created by Duc Nguyen on 3/21/17.
//  Copyright © 2017 Axon Active. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var qrCodeImage: UIImageView!
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        generateTestString()
    }
    
    func generateTestString() {
        let currentTime = Date()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let date = formatter.string(from: currentTime)
        formatter.dateFormat = "HH:mm:ss"
        let time = formatter.string(from: currentTime)
        
        let string = String(format:"Current Date: %@ - Current Local Time: %@", date, time)
        
        if let image = string.generateQRCode() {
            qrCodeImage.image = image
        }
    }
    
    @IBAction func unwindToMainScreen(segue: UIStoryboardSegue) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func generateNewCode(_ sender: Any) {
        generateTestString()
    }
}
