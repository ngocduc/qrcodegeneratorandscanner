//
//  UIColor+Extensions.swift
//  QRCodeResearch
//
//  Created by Duc Nguyen on 3/22/17.
//  Copyright © 2017 Axon Active. All rights reserved.
//

import UIKit

extension UIColor {
	convenience init(hex: UInt, alpha: CGFloat = 1) {

		var red, green, blue: UInt

		red = ((hex & 0x00FF0000) >> 16)
		green = ((hex & 0x0000FF00) >> 8)
		blue = hex & 0x000000FF

		self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: alpha)
	}
}

