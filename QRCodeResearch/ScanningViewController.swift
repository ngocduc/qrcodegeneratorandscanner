//
//  ScanningViewController.swift
//  QRCodeResearch
//
//  Created by Duc Nguyen on 3/21/17.
//  Copyright © 2017 Axon Active. All rights reserved.
//

import UIKit
import AVFoundation

class ScanningViewController: UIViewController {
    
    @IBOutlet fileprivate weak var messageLabel:UILabel!
    @IBOutlet fileprivate weak var topView: UIView!
	@IBOutlet fileprivate weak var bottomView: UIView!
    
    fileprivate var qrCodeFrameView: UIView?
    fileprivate var captureSession: AVCaptureSession?
    fileprivate var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    fileprivate var lastString = ""
    fileprivate let supportedCodeTypes = [AVMetadataObjectTypeUPCECode,
                              AVMetadataObjectTypeCode39Code,
                              AVMetadataObjectTypeCode39Mod43Code,
                              AVMetadataObjectTypeCode93Code,
                              AVMetadataObjectTypeCode128Code,
                              AVMetadataObjectTypeEAN8Code,
                              AVMetadataObjectTypeEAN13Code,
                              AVMetadataObjectTypeAztecCode,
                              AVMetadataObjectTypePDF417Code,
                              AVMetadataObjectTypeQRCode]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    fileprivate func setup() {
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            captureSession = AVCaptureSession()
            captureSession?.addInput(input)
            
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            
            videoPreviewLayer = {
                $0?.videoGravity = AVLayerVideoGravityResizeAspectFill
                $0?.frame = view.layer.bounds
                return $0
            }(AVCaptureVideoPreviewLayer(session: captureSession))
            
            view.layer.addSublayer(videoPreviewLayer!)
            
            captureSession?.startRunning()
        
            view.bringSubview(toFront: topView)
            
            qrCodeFrameView = UIView()
            
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
                qrCodeFrameView.layer.borderWidth = 2
                view.addSubview(qrCodeFrameView)
                view.bringSubview(toFront: qrCodeFrameView)
            }
            
        } catch {
            print(error.localizedDescription)
        }
    }

	fileprivate func fadeIn() {
		UIView.animate(withDuration: 0.2){ [unowned self] _ in
			self.bottomView.backgroundColor = .green
			self.bottomView.alpha = 1
		}
	}

	fileprivate func fadeOut(){
		UIView.animate(withDuration: 0.2){ [unowned self] _ in
			self.bottomView.backgroundColor = .lightGray
			self.bottomView.alpha = 0.6
		}
	}
}

extension ScanningViewController: AVCaptureMetadataOutputObjectsDelegate {
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        guard let metadataObjects = metadataObjects, !metadataObjects.isEmpty else {
            qrCodeFrameView?.frame = .zero
            messageLabel.text = "Start scanning Tickets now"
            lastString = ""
			fadeOut()
            return
        }
        
        guard let metadataObj = metadataObjects[0] as? AVMetadataMachineReadableCodeObject else { return }
        
        if supportedCodeTypes.contains(metadataObj.type) {
            
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if let string = metadataObj.stringValue {
                messageLabel.text = string
                if string != lastString {
					fadeIn()
                    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
                    lastString = string
                }
            }
        }
    }
}
