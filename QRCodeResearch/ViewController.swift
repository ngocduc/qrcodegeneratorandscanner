//
//  ViewController.swift
//  QRCodeGeneratorAndReaderExample
//
//  Created by Duc Nguyen on 3/21/17.
//  Copyright © 2017 Axon Active. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet fileprivate weak var outputImage: UIImageView!
    @IBOutlet fileprivate weak var inputTextfield: UITextView!
    @IBOutlet fileprivate weak var correctionLevel: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inputTextfield.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        generateTestString()
    }
}

// MARK: Actions
extension ViewController {
    
    @IBAction func generate(_ sender: Any) {
        guard !inputTextfield.text.isEmpty, let level = CorrectionLevel(rawValue: correctionLevel.selectedSegmentIndex) else {
            generateTestString()
            return
        }
        if let image = inputTextfield.text.generateQRCode(withCorrection: level) {
            outputImage.image = image
        }
    }
    
    @IBAction func changeCorrectionLevel(_ sender: Any) {
        generate(sender)
    }
    
    @IBAction func clear(_ sender: Any) {
        inputTextfield.text = ""
        outputImage.image = nil
    }
    
    @IBAction func scan(_ sender: Any) {
        
    }
}

// MARK: Privates
fileprivate extension ViewController {
    
    func generateTestString() {
        let currentTime = Date()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let date = formatter.string(from: currentTime)
        formatter.dateFormat = "HH:mm:ss"
        let time = formatter.string(from: currentTime)
        
        let string = String(format:"Current Date: %@ - Current Local Time: %@", date, time)
        inputTextfield.text = string
        
        generate(correctionLevel)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension ViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        outputImage.image = nil
    }
}

enum CorrectionLevel: Int {
    case L = 0
    case M = 1
    case Q = 2
    case H = 3
}

extension String {
    
    func generateQRCode(withCorrection level: CorrectionLevel = .M, forFrame size: CGRect = .zero) -> UIImage? {
        let data = self.data(using: .utf8)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            filter.setValue(String(describing: level), forKey: "inputCorrectionLevel")
            
            guard let outputImage = filter.outputImage else { return nil }
            
            var transform: CGAffineTransform
            
            if size != .zero {
                let scaleX = size.width / outputImage.extent.size.width
                let scaleY = size.height / outputImage.extent.size.height
                
                transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
            } else {
                transform = CGAffineTransform(scaleX: 100, y: 100)
            }
            
            let ciImage = outputImage.applying(transform)
            
            return UIImage(ciImage: ciImage)
        }
        return nil
    }
}
